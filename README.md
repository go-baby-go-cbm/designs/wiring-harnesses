# Wiring Harnesses

![Wiring Kit](supplemental/completed-harnesses.jpg)

## Overview

This repository contains instructions and guidelines for building wiring harnesses for Go Baby Go 
car builds and related materials. All harnessing is documented within the provided 
<[harnesses](harnesses)> and <[supplemental](supplemental)> folders.

For specifics on how to use the harnessing documentation provided, please see the 
[README](harnesses/README.md) for harnesses.


## How to Use this Project

Use the PowerPoint and any supplemental photos provided to fabricate the harnesses needed for your 
Go Baby Go car build.


## Harnesses Included

The following harnesses are documented within this repository:

-   Battery
    -   Purpose:
        -   Harness used to attach a T-Plug to a car battery.
    -   Materials:
        -   [battery.pptx](harnesses/battery.pptx)
        -   [Supplemental Photos](supplemental/battery)

-   Charging Plug
    -   Purpose:
        -   Charging plug which mates with the charging port.
        -   Note that this may not be neccessary as the provided car's charging plug may fit the 
            charging port detailed in this guide.
    -   Materials:
        -   [charging-plug.pptx](harnesses/charging-plug.pptx)

-   Go Button Jacks
    -   Purpose:
        -   Attachment points for the Go Button.
    -   Materials:
        -   [go-button-jacks.pptx](harnesses/go-button-jacks.pptx)
        -   [Supplemental Photos](supplemental/go-button-jacks)

-   Kill Switch & Charging Port
    -   Purpose:
        -   Provides a physical kill switch and accessible charging port.
    -   Materials:
        -   [kill-switch-and-charging-port.pptx](harnesses/kill-switch-and-charging-port.pptx)
        -   [Supplemental Photos](supplemental/kill-switch-and-charging-port)

-   Relay
    -   Purpose:
        -   Power relay that can be controlled by a Go Button to drive a car.
    -   Materials:
        -   [relay.pptx](harnesses/relay.pptx)
        -   [Supplemental Photos](supplemental/relay)

-   Undercarriage - Power
    -   Purpose:
        -   Route power through the undercarriage of the car.
    -   Materials:
        -   [undercarriage--power.pptx](harnesses/undercarriage--power.pptx)
        -   [Supplemental Photos](supplemental/undercarriage--power)


## Version Information

This is version 1.0 of this guide. Please see the [Changelog](Changelog.md) for a detailed version 
history.


## License

This project and materials are released under the [Unlicense](LICENSE). We provide the materials 
included here as a reference to anyone working in the Go Baby Go space. We hope this serves as a 
stepping stone to improved designs and would love to hear back on any improvements made on what we 
have created here.


