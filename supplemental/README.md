# Overview

This directory contains files to help with debugging and wiring up new harnesses. The photos 
included are from previous wiring sessions for harnesses.

Please note that not all harnesses have suplemental photos but if they do, the folder names 
included here correlate 1:1 to the harness documents in this repository.


