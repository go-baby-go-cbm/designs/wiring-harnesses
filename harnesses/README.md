# Overview

Each harness is documented individually via a PowerPoint presentation. If there are any supplemental 
materials (i.e. photos), they can be found in a folder matching the name of the PowerPoint file in 
the <[supplemental](supplemental)> folder (in the root of this project).

Each harness is documented in a standalone PowerPoint which includes the following sections:

-   Cover Page:
    -   Contains version information and a photo of a completed harness.

-   Overview:
    -   The intended usage for the harness.

-   Materials:
    -   Summarizes all materials needed to fabricate the harness
    -   Please keep the following in mind when viewing the table of materials:
        -   The fabrication notes will refer to materials by the 'ID' listed in this section.
        -   The 'MATERIAL' identifier provided corresponds to an entry in the 
            [materials.xlsx](materials.xlsx) spreadsheet.
        -   The 'VARIANT(S)' list which (if any) specific variant in the spreadhseet should be used 
            when creating the harness. If multiple are provided, any can be used. If no variant is 
            indicated, then there is only one variant available in the spreadsheet or any variant 
            listed can be used.

-   Wiring Diagram:
    -   Details the connectivity of a harness.
    -   Most harnesses do not have this but if they are complex enough this will be provided.

-   Key Dimensions:
    -   A photo of the harness with key dimensions labeled.
    -   Note:  This section is not always provided as it is not always necessary.

-   Fabrication Process:
    -   Details the suggested steps for how to wire the harness.


